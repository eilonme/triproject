import 'package:flutter/material.dart';
import 'package:triproject/models/user.dart';
import 'package:triproject/screen/home_and_maps/home/home.dart';
import 'package:triproject/screen/wrapper.dart';
import 'package:triproject/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:triproject/screen/home_and_maps/video_map_show/home_map_show.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthServices().user,
      child: MaterialApp(
        // Start the app with the "/" named route. In this case, the app starts
        // on the FirstScreen widget.
        initialRoute: '/',
        routes: {
          // When navigating to the "/" route, build the FirstScreen widget.
          '/yt': (context) => MapsHomeScreen(),
          '/h': (context) => Home(),
          '/': (context) => Wrapper()
        },
      ),
    );
  }
}
