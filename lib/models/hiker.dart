class Hiker {

  final String name;
  final int timeHiked;
  final bool guide;

  Hiker({ this.name, this.guide, this.timeHiked});

}