import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:triproject/models/hiker.dart';
import 'package:triproject/models/user.dart';
import 'package:triproject/models/user.dart';
import 'package:triproject/models/hiker.dart';

class DatabaseService {
  
  final String uid;
  DatabaseService({this.uid});

  //collection
  final CollectionReference hikerCollection = Firestore.instance.collection('hikers');

  Future updateUserData(int timeHiked, String name, bool guide) async {
    return await hikerCollection.document(uid).setData({
        'timeHiked': timeHiked,
        'name': name,
        'guide': guide,
    });
  }


  //hiker list
  List<Hiker> _hikerListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc){
      return Hiker(
        name: doc.data['name'] ?? '',
        timeHiked: doc.data['timeHiked'] ?? 0,
        guide: doc.data['guide'] ?? false
      );
    }).toList();
  }

  //userData from snap
  UserData _userDataFromSnap(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: snapshot.data['name'],
      timeTraveld: snapshot.data['timeTraveld']
    );
  }
  //gett hikers stream
  Stream<List<Hiker>> get hikers {
    return hikerCollection.snapshots()
      .map(_hikerListFromSnapshot);
  }

  //get user doc
  Stream<UserData> get userData {
    return hikerCollection.document(uid).snapshots()
    .map(_userDataFromSnap);
  }
   
}