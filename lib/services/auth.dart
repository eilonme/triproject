import 'package:firebase_auth/firebase_auth.dart';
import 'package:triproject/models/user.dart';
import 'package:triproject/services/database.dart';

class AuthServices{

  final FirebaseAuth _auth = FirebaseAuth.instance;

  //crate

  User _userFromFirebaseUser(FirebaseUser user){
    return user != null ? User(uid: user.uid) : null;
  }

  //straem

  Stream<User> get user{
    return _auth.onAuthStateChanged
      .map(_userFromFirebaseUser);
  }

  //sgin in anon

  // Future SignInAnon() async{
  //   try{
  //     AuthResult result = await _auth.signInAnonymously();
  //     FirebaseUser user = result.user;
  //     return _userFromFirebaseUser(user);                                         
  //   }catch(e){
  //     print(e.toString());
  //     return null;
  //   }
  // }

  //sign in

  Future signInWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser firebaseUser = result.user;
      return _userFromFirebaseUser(firebaseUser);
    }catch(e){
      print(e.toString());
      return null;
    }
  } 


  //rej

  Future regisretWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser firebaseUser = result.user;

      //crate document
      await DatabaseService(uid: firebaseUser.uid).updateUserData(0, 'new hiker', false);
      return _userFromFirebaseUser(firebaseUser);
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  //sign out

  Future signOut() async {
    try {
      return await _auth.signOut();
    }catch(e){
      print(e.toString());
      return null;
    }
  }

}