import 'package:flutter/material.dart';
import 'package:triproject/screen/authenticate/authenticate.dart';
import 'package:triproject/screen/home_and_maps/home/home.dart';
import 'package:archive/archive.dart';
import 'package:provider/provider.dart';
import 'package:triproject/models/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    if(user == null){
      return Authenticate();
    } else {
      return Home();
    }

  }
}