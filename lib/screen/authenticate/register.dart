import 'package:flutter/material.dart';
import 'package:triproject/services/auth.dart';
import 'package:triproject/shared/constants.dart';
import 'package:triproject/shared/loading.dart';

class Register extends StatefulWidget {

  final Function toggleView;
  Register({ this.toggleView });

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthServices _authServices = AuthServices();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;


  //text
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
        backgroundColor: Colors.green[100],
        appBar: AppBar(
          backgroundColor: Colors.green[400],
          elevation: 0.0,
          title: Text('sign up To Trip!', style: TextStyle(color: Colors.yellowAccent[500])),
          actions: <Widget>[
            FlatButton.icon(
              onPressed: () {
                widget.toggleView();
              },
              icon: Icon(Icons.person),
              label: Text('Sign me in'),
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: textInputDecoration.copyWith(hintText: 'Email', ),
                  validator: (val) => val.isEmpty ? 'Enter Email' : null,
                  onChanged: (val) {
                    setState(()=> email = val);
                  }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: textInputDecoration.copyWith(hintText: 'Password'),
                  validator: (val) => val.length < 6 ? 'Enter  6  + chars' : null,
                  obscureText: true,
                  onChanged: (val) {
                    setState(()=> password = val);
                  }
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.brown[400],
                  child: Text(
                    'Register',
                    style: TextStyle(color: Colors.cyan),
                  ),
                  onPressed: () async{
                    if(_formKey.currentState.validate()){
                      setState(() {
                        loading = true;
                      });
                      dynamic result = await _authServices.regisretWithEmailAndPassword(email, password);
                      if(result == null){
                         setState(() {
                           error = 'Enter vaild Email';
                           loading = false;
                         });
                      }
                    }
                  }
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.lightGreen[10], fontSize: 14.0)
                )
              ]
            )
            ),
        ),
    );
  }
}