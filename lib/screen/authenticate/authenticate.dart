import 'package:flutter/material.dart';
import 'package:triproject/screen/authenticate/register.dart';
import 'sign_in.dart';
import 'package:triproject/screen/authenticate/sign_in.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = true;
  void toggleViewSR(){
    setState(() => showSignIn = !showSignIn);
  }



  @override
  Widget build(BuildContext context) {
    if(showSignIn){
      return SignIn(toggleView: toggleViewSR);
    } else {
      return Register(toggleView: toggleViewSR);
    }
  }
}