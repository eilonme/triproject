import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:triproject/models/hiker.dart';
import 'hiker_tile.dart';

class HikerList extends StatefulWidget {
  @override
  _HikerListState createState() => _HikerListState();
}

class _HikerListState extends State<HikerList> {
  @override
  Widget build(BuildContext context) {

    final hikers = Provider.of<List<Hiker>>(context)  ?? [];
      

    return ListView.builder(
      itemCount: hikers.length,
      itemBuilder: (context, index) {
        return HikerTile(hiker: hikers[index]);
      },
    );
  }
}