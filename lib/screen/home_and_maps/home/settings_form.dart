import 'package:flutter/material.dart';
import 'package:triproject/services/database.dart';
import 'package:triproject/shared/constants.dart';
import 'package:triproject/models/user.dart';
import 'package:triproject/shared/loading.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  final _formKey = GlobalKey<FormState>();
  final List<String> timeTraveld = ['10','20','30','40','50','60','70'];

  //form values
  String _currentName =  '';
  int _currentTimeTraveld = 100 ;
  bool _currentGuideMode = false;

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {

          UserData userData = snapshot.data;

          return Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Text(
                  'Update your hiker settings:',
                 style: TextStyle(fontSize: 18.00),
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  initialValue: userData.name,
                  decoration: textInputDecoration.copyWith(
                    hintText: 'Name:' + ' ' + _currentName
                  ),
                  validator: (val) => val.isEmpty ? 'Enter your name' : null,
                  onChanged: (val) {
                    setState(()=> _currentName = val);
                 }
                ),
                SizedBox(height: 20.0), 
   
                //slider
                Slider(
                  value: (_currentTimeTraveld ?? userData.timeTraveld).toDouble(),
                  activeColor: Colors.lime[_currentTimeTraveld ?? userData.timeTraveld],
                  inactiveColor: Colors.lightGreen[_currentTimeTraveld ?? userData.timeTraveld],
                  min: 100.0,
                  max: 900.0,
                  divisions: 8,
                  onChanged: (val) => setState(() => _currentTimeTraveld = val.round()),
                  ),
                 
                Text(
                  'Guide mode',
                  style: TextStyle(fontSize: 11.00)
                ),
                Checkbox(
                  value: (_currentGuideMode ?? userData.guideMode),
                  activeColor: Colors.green[_currentTimeTraveld ?? userData.timeTraveld], 
                  focusColor: Colors.lime[_currentTimeTraveld ?? userData.timeTraveld],
                  checkColor: Colors.blue[600],
                  hoverColor: Colors.brown[100],
                  onChanged: (val) => setState(() => _currentGuideMode = val)
                ),

                RaisedButton(
                  color: Colors.green[200],
                  child: Text(
                    'Save',
                   style: TextStyle(color: Colors.blue),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      await DatabaseService(uid: user.uid).updateUserData(
                        _currentTimeTraveld ?? userData.timeTraveld,
                        _currentName ?? userData.name,
                        _currentGuideMode ?? userData.guideMode
                        );
                        Navigator.pop(context);
                    }
                    //print(_currentName);
                    //print(_currentTimeTraveld);
                 }
                ),
             ]
            ),
          );
        } else {
          return Loading();
        }
        
      }
    );
  }
}