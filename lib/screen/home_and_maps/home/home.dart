import 'package:flutter/material.dart';
import 'package:triproject/screen/home_and_maps/home/settings_form.dart';
import 'package:triproject/services/auth.dart';
import 'package:triproject/services/database.dart';
import 'package:provider/provider.dart';
import 'package:triproject/screen/home_and_maps/home/hiker_list.dart';
import 'package:triproject/models/hiker.dart';
import 'package:slider_button/slider_button.dart';

class Home extends StatelessWidget {
  final AuthServices _authServices = AuthServices();

  @override
  Widget build(BuildContext context) {
    void _showSettingsPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              child: SettingsForm(),
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
            );
          });
    }

    return StreamProvider<List<Hiker>>.value(
      value: DatabaseService().hikers,
      child: Scaffold(
        backgroundColor: Colors.green[50],
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('Welcome to Trip',
              style: TextStyle(color: Colors.yellowAccent[500])),
          backgroundColor: Colors.green[400],
          elevation: 0.0,
          actions: <Widget>[
            FlatButton.icon(
                onPressed: () async {
                  await _authServices.signOut();
                },
                icon: Icon(Icons.person),
                label: Text('logout')),
            FlatButton.icon(
                onPressed: () => _showSettingsPanel(),
                icon: Icon(Icons.nature),
                label: Text('settings'))
          ],
        ),
        body: Stack(
          children: <Widget>[
            Background(),
            Forground(),
          ],
        ),
      ),
    );
  }
}

class Forground extends StatelessWidget {
  const Forground({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(child: HikerList()),
        Container(
          // child: RaisedButton(
          //   child: Text(""), 
          //   onPressed: (){
          //     Navigator.pushNamed(
          //       context, 
          //       '/yt'
          //     );  
          //   },
          // ),
          child: SlideToMaps(),
          width: double.infinity,
        ),
      ],
    );
  }
}

class SlideToMaps extends StatelessWidget {
  const SlideToMaps({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliderButton(
    vibrationFlag: false,
    action: () {
      Navigator.pushNamed(context, '/yt');
    },
      label: Text(
        "Slide to TRIP !",
        style: TextStyle(
            color: Colors.green, fontWeight: FontWeight.w500, fontSize: 17),
      ),
      icon: Center(
        child: Icon(
          Icons.nature_people,
          color: Colors.yellow[100],
          size: 10.0,
          semanticLabel: 'Text to announce in accessibility modes',
        ),
      ),

      ///Change All the color and size from here.
      width: 230,
      radius: 10,
      buttonColor: Colors.lime[500],
      backgroundColor: Colors.lightGreen[200],
      highlightedColor: Colors.blue,
      baseColor: Colors.lightGreenAccent[700],
    );
  }
}

class Background extends StatelessWidget {
  const Background({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/crop.php.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
