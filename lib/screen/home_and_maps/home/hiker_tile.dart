import 'package:flutter/material.dart';
import 'package:triproject/models/hiker.dart';

class HikerTile extends StatelessWidget {

  final Hiker hiker;
  HikerTile({ this.hiker });

  @override
  Widget build(BuildContext context) {
    return Padding(

      padding: EdgeInsets.only(top: 8.0),
      child: Card(
        margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0.0),
        child: ListTile(
          leading: CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.green[hiker.timeHiked ?? 100],
            backgroundImage: AssetImage('assets/asset_3.png'),
          ),
          title: Text(hiker.name),
          subtitle: hiker.guide ? Text('GUIDE!', style: TextStyle(color: Colors.lightGreen[700]),) : Text('HIKER!', style: TextStyle(color: Colors.blue[600])),
        ),
      ),
    );
  }
}
