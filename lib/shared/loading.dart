import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_spinkit/src/ripple.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueAccent[170],
      child: Center(
        child: SpinKitRipple(
          color: Colors.cyan,
          size: 50.0,
        )
      ),
    );
  }
}